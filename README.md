# OpenStack on Kubernetes

An umbrella chart managing CERN's OpenStack deployment on Kubernetes.

## Clusters

Clusters are currently under *Cloud Infra Services* - named following
convention *cci-openstack-plane-ha-xxx*.

## Configuration

The full configuration for all services and regions is contained in this
repository. The structure is as follows:
* charts/openstack/values.yaml: common configuration for all regions
* releases/<region-name>/values.yaml: specific configuration for each region
(overrides common config)

Since these files contain encrypted values (sensitive data) you should not edit
them directly, but using the sops client instead - also making sure you have 
the *Cloud Infra Services* project configured as it holds the encryption key.

Fetch the sops client if you haven't already:
```bash
wget https://gitlab.cern.ch/cloud/sops/-/jobs/8834328/artifacts/raw/sops?inline=false
```

Generate an openstack token so you can use sops below:
```bash
export OS_TOKEN=$(openstack token issue -c id -f value)
```

Branch, make changes, commit, open MR:
```bash
git checkout -b mychange

sops releases/cern/values.yaml
( edit )

git add releases/cern/values.yaml
git commit -m 'a meaningful error message'
git push origin mychange
```

## Deployment

We use Flux to keep the clusters up to date with the latest configuration - the
detailed workflow of how this works can be
[found here](https://gitlab.cern.ch/helm/releases/gitops-getting-started). It
makes sure all changes applied in this repository are propagated to all
clusters running our deployment.

You only need to deploy Flux once for every new cluster. It will take care of
keeping the cluster deployment in sync with the configuration in this repository.

There are two components that need to be deployed - Flux and the Helm Operator.

```bash
helm upgrade -i helm-operator fluxcd/helm-operator --namespace flux \
    --version 1.1.0 --values helm-operator-values.yaml

helm upgrade -i flux fluxcd/flux --namespace flux \
    --version 1.3.0 \
    --set git.url=https://gitlab.cern.ch/helm/releases/openstack \
    --values flux-values.yaml
```

Verify the deployment looks ok (after flux runs you should see one additional
deployment per region):
```bash
helm ls -A
```

## Troubleshooting

Check the [gitops-getting-started](https://gitlab.cern.ch/helm/releases/gitops-getting-started) for generic troubleshooting of flux and the helm operator
